<div class="abstract">
The BusPlus project aims at improving the off-peak hours public transit service in Canberra, Australia. To address the difficulty of covering a large geographic area, BusPlus proposes a hub and shuttle model consisting of a combination of a few high-frequency bus routes between key hubs and a large number of shuttles that bring passengers from their origin to the closest hub and take them from their last bus stop to their destination. This note presents the dataset used in <span id="f9b9a34dd5b6dcffa5534cb2131b2435"><a href="#maheo2017benders" title="Benders Decomposition for the Design of a Hub and Shuttle Public Transit System">Mahéo et al. (2017)</a></span> to solve this public network design.

</div>


# Table of Contents

1.  [Introduction](#org8471535)
2.  [Data Format](#org5389b06)
    1.  [Distance Matrix](#org07e9d40)
    2.  [Trip Requests](#org5177d65)
    3.  [Hubs](#org11fd84c)
3.  [Citing](#org9636a7c)


<a id="org8471535"></a>

# Introduction

Canberra is a planned city designed in 1913 by American architect Walter Burley Griffin. It is built as a collection of semi-autonomous towns surrounded by greenbelts. As such, the city covers a very wide geographic area. This creates a challenging environment for public transportation as, apart for a couple city centres, the density of population is very low.

In the current situation Action, Canberra&rsquo;s public transport operator, has to run ninety-four bus lines linking over two thousand seven hundred bus stops. In practice, most of the time, these buses run empty. This leads to two issues:

1.  the cost of running the system is very high, as there is need for many buses to be active at a given time; and
2.  the quality of service is low because it is not possible to run nigh empty vehicles very often.

The BusPlus project aimed[^1] at addressing this issue by creating a Hub-and-Shuttle transportation network. In a hub-and-shuttle system, buses are few but run on major corridors at regular intervals. Theses buses are then supplemented by shuttles, or taxis, which take care of bringing passengers to and from their requested stops.

In the typical use-case, a customer would book a trip using an online tool. She would then be instructed to go to her nearest bus stop within fifteen minutes. Here a taxi would pick her hub and bring her to a bus *hub*. From there she would connect to a waiting bus which would drive her most of the way. When she would get off the bus, another taxi would be waiting and complete her journey.

The BusPlus project was aimed at off-peak times. Indeed, demand patterns are very clear in Canberra: a morning, end-of-school, and evening rush &#x2013; as can be seen in [fig.1](#trips).

![Figure 1: Bar chart of the number of trips per time of day, grouped in five minutes slices, for a Monday.](trips.svg)

This note aims at presenting the data used during this project. Results using a Benders decomposition for this network design problem are published in <span id="f9b9a34dd5b6dcffa5534cb2131b2435"><a href="#maheo2017benders" title="Benders Decomposition for the Design of a Hub and Shuttle Public Transit System">Mahéo et al. (2017)</a></span>.


<a id="org5389b06"></a>

# Data Format

The data comes in three types of files:

1.  One distance matrix file: `disttime.csv`
2.  A folder with one month of trip requests as CSV files: `days/`
3.  One hub file: `10hubs.dat`


<a id="org07e9d40"></a>

## Distance Matrix

The distance matrix is a (large) file containing the distance and time between two given stops on the network. Therefore, it provides more accurate information than simple Euclidean distance or such. It is formatted as follows:

-   `Dep`: starting bus stop;
-   `Arr`: destination stop;
-   `m`: distance in meters between the two stops;
-   `s`: time in seconds between the two stops.


<a id="org5177d65"></a>

## Trip Requests

The trip requests are a collection of thirty files corresponding to a month of trips. Demands have been anonymised[^2] and randomised.[^3] In each file, requests are defined using three fields:

-   `time`: the request time as a six-character string, times before 10am are prefixed with &rsquo;0&rsquo;;
-   `dep`: the origin stop;
-   `arr`: the destination stop.


### Bus Stations

![Figure 2: The City Bus Station in Canberra is a collection of platforms, we represent such a station as a single stop](TC_station_city.svg)

The Action network revolves around a number of bus *stations.* These stations are collection of platforms which are the actual bus stops. To represent flow more accurately we aggregate all platforms under a single stop.[^4] For example, [fig. 2](#fig:civic) shows the layout of the &ldquo;City Bus Station,&rdquo; which comprises nine different platforms. In the data, the entire station is identified as stop id: 1723.


<a id="org11fd84c"></a>

## Hubs

The BusPlus project aim is to design new bus lines running between selected *nodes* in the network. These are bus stops selected using a *p*-median and provided to us. The hub file list a hub per line.


<a id="org9636a7c"></a>

# Citing

To cite this dataset, please refer to the original paper and either the arXiv document[^5] or the URL of the repository.[^6]

# Bibliography

<a id="maheo2017benders"></a>Mahéo, Kilby & Van Hentenryck, Benders Decomposition for the Design of a Hub and Shuttle Public Transit System, <i>Transportation Science</i>, (2017). <a href="https://doi.org/10.1287/trsc.2017.0756">link</a>. [↩](#f9b9a34dd5b6dcffa5534cb2131b2435)

[^1]: BusPlus was discontinued in favour of a light rail project.

[^2]: Removed identifier as well as trips&rsquo; date in filenames.

[^3]: $`\pm 5`$ minutes.

[^4]: I.e., an entire station is reduced to a single stop.

[^5]: <https://arxiv.org/abs/1601.00367>

[^6]: <https://gitlab.com/Soha/busplus-dataset>
